const rp = require('request-promise');
const cheerio = require('cheerio');

const options = {
    url: 'http://www.unesc.net/portal/capa/index/220/',
    transform: function (body) {
        return cheerio.load(body);
    }
};

function processarDados(dados){
    //salva no banco de dados
    console.log(JSON.stringify(dados));
}

rp(options)
    .then( ($) => {
        const setores = [];
        $('#listaOS .clearfix ul li').each((i, item) => {
            const setor = {
                nome: $(item).find('h4').text()
            };
            if(setor.nome !== ''){
                setores.push(setor);
            }
        });
        processarDados(setores);
    })
    .catch( (err) => {
        console.log(err);
    });